'use strict';

const isHome = $network.wifi.ssid === 'Yihang\'s WiFi' ||
  $network.wifi.ssid === 'Dada' ||
  $network.wifi.ssid === 'Office WiFi';

if (isHome) {
  $done({
    servers: ['192.168.1.147'],
  });
} else {
  $done({
    servers: ['119.29.29.29', '223.5.5.5'],
  });
}

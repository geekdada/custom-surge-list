$httpClient.get('http://119.29.29.29/d?ttl=1&dn=' + $domain, function(
  error,
  response,
  data
) {
  if (error) {
    $done({}); // Fallback to standard DND query
  } else {
    const pair = data.split(',');
    const ips = pair[0];
    const ttl = pair[1];
    $done({ addresses: ips.split(';'), ttl: Number(ttl) });
  }
});
